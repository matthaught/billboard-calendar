<?php 

require_once('config.inc.php');
require_once 'Zend/Loader.php';
Zend_Loader::loadClass('Zend_Gdata');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
Zend_Loader::loadClass('Zend_Gdata_Calendar');
Zend_Loader::loadClass('Zend_Http_Client');

$service = new Zend_Gdata_Calendar();

$gdata_events = array();

foreach ($feeds as $feed) {

    $query = $service->newEventQuery();
    $query->setUser($feed['calendar']);
    $query->setVisibility($feed['cookie']);
    $query->setProjection('full');
    $query->setOrderby('starttime');
    $query->setStartMin(date('Y-m-d'));
    $query->setStartMax(date('Y-m-d', strtotime(date('Y-m-d'))+(60*60*24*14)));
    
    $eventFeed = $service->getCalendarEventFeed($query);
    
    foreach ($eventFeed as $event) {
        $gdata_events[] = array(
            'title' => $event->title->text,
            'where' => str_replace('NCSU ', '', $event->where[0]->valueString),
            'start' => strtotime($event->when[0]->startTime),
            'end' => strtotime($event->when[0]->endTime),
            'eventid' => $event->id->text
        );
    }
}

usort($gdata_events, function($a, $b) {
    return ($a['start']) - ($b['start']);
});

?>
<html>
<head>
<title>Math Event Calendar</title>
<link href="esign.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="content">
    <table>

    <?php $i=0; foreach ($gdata_events as $row) { ?>

<?php
if ($i % 2 == 0) {
    $class="light";
} else {
    $class="dark";
}
$date = null;
$time = null;
$today = null;
// dates

if (($row['end'] - $row['start']) <= 86400 && ($row['end'] - $row['start']) > 80000) {
    // all day
    $date = date('D, M jS', $row['start']);
    $time = 'All Day';
} elseif (date('d', $row['start']) != date('d', $row['end'])) {
    // multi day
    $date = date('D, M jS', $row['start']) .' - ' . date('D, M jS', $row['end']);
    $time = null;
} else {
    $date = date('D, M jS', $row['start']);
    $time = date('g:i', $row['start']) . ' - ' . date('g:iA', $row['end']);
}

if (date("Y-m-d", $row['start']) == date("Y-m-d",time())) {
    $today = 'highlight';
}


?>


    <tr class="<?php echo $class; ?> <?php if ($i === 0) { echo 'first'; } ?>">
        <td align="center" class="datetime <?php echo $today; ?>"<?php if ($time === null) { echo ' colspan="2"'; } ?>><font size="+2"><?php echo $date; ?></font></td>
        <?php if ($time !== null) { ?>
        <td align="center" class="datetime <?php echo $today; ?>"><font size="+2"><?php echo $time; ?></font></td>
        <?php } ?>
        <td class="info <?php echo $today; ?>"><font size="+2"><?php echo $row['title']; ?></font></td>
        <td class="info <?php echo $today; ?>"><font size="+2"><?php echo $row['where']; ?></font></td>
    </tr>                                    
    <?php 
$i++;
if ($i >= $maxrows) { 
    break;
}
?>
    <?php } ?>
</table>

</div>
</body>
</html>
